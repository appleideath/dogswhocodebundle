<?php
   /*
   Plugin Name: Dogswhocode Developer Bundle
   Plugin URI: https://dogswhocode.com
   Description: Customized functions intended for geekydevils wordpress development.
   Version: 1.0
   Author: DOGE TERRIFIED
   Author URI: https://www.facebook.com/appleideath
   License: GPL2
   */
   
	
	DEFINE('PLUGIN_URL',plugins_url());
	DEFINE('PLUGIN_URL_JS',plugins_url('/dev/assets/js/'));
	DEFINE('PLUGIN_URL_CSS',plugins_url('/dev/assets/css/'));
	
   include( plugin_dir_path( __FILE__ ) . 'lib/widget-class.php');
   include( plugin_dir_path( __FILE__ ) . 'lib/button-widget.php');
   include( plugin_dir_path( __FILE__ ) . 'lib/image-widget.php');
   include( plugin_dir_path( __FILE__ ) . 'lib/image-hover.php');
   /*TO BE UPDATED*/
   #include( plugin_dir_path( __FILE__ ) . 'lib/testimonial-widget.php');
   #include( plugin_dir_path( __FILE__ ) . 'lib/lorem.php');
   

	add_action( 'admin_enqueue_scripts', 'db_widget_enqueue_scripts' );
	function db_widget_enqueue_scripts( $hook ){
		// Image Upload
		wp_enqueue_media();
		wp_enqueue_script( 'widget-image-upload', PLUGIN_URL.'/dev/assets/js/image-upload.js', array( 'jquery' ) );
	}
	
	function add_my_stylesheet(){
    wp_enqueue_style( 'myCSS', plugins_url( '/assets/css/app.css', __FILE__ ) );
	}
	add_action('wp_enqueue_scripts', 'add_my_stylesheet');
	
	
	// Before Testimonial Content
	function db_testimonial_before_content( $instance ) {
		do_action( 'db_testimonial_before_content', $instance );
	}

	// After Testimonial Content
	function db_testimonial_content( $instance ) {
		do_action( 'db_testimonial_content', $instance );
	}

	// After Testimonial Content
	function db_testimonial_after_content( $instance ) {
		do_action( 'db_testimonial_after_content', $instance );
	}

	// After Testimonial Loop
	function db_testimonial_after_loop( $instance ) {
		do_action( 'db_testimonial_after_loop', $instance );
	}


   
   